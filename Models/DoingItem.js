'use strict';

function DoingItem(doingItem) {
  this.summary = doingItem;
}

DoingItem.prototype.getDoingItems = function() {
  return this.summary;
};

DoingItem.prototype.setCheckListId = function(checkListId) {
  this.checkListId = checkListId;
};

DoingItem.prototype.getCheckListId = function() {
  return this.checkListId;
};

DoingItem.prototype.setCheckList = function(checkList) {
  this.checkList = checkList;
};

DoingItem.prototype.getCheckList = function() {
  return this.checkList;
};

module.exports = DoingItem;
