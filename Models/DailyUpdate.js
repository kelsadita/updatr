'use strict';

function DailyUpdate(doneItems, doingItems) {
  this.doneItems = doneItems;
  this.doingItems = doingItems;
}

module.exports = DailyUpdate;
