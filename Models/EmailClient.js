'use strict';

function EmailClient(user, password, scrumMasterEmail, fromAddress) {
  this.user = user;
  this.password = password;
  this.scrumMasterEmail = scrumMasterEmail;
  this.defaultFromAddress = fromAddress;
}

module.exports = new EmailClient();
