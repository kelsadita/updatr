'use strict';

function DoneItem(doneItem) {
  this.summary = doneItem;
}

DoneItem.prototype.getDoneItems = function() {
  return this.summary;
};

module.exports = DoneItem;
