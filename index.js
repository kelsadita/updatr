#!/usr/bin/env node

'use strict';

// Utilities
var program = require('commander'),
    fs = require('fs');

// Models
var EmailClient = require('./Models/EmailClient');

// Controllers
var updateEmailController = require('./Controllers/updateEmailController');

// Trello specific variables
var Trello = require('node-trello'),
    TrelloClient;

(function init() {

  program
    .version('0.0.1')
    .usage('[config-file]')
    .parse(process.argv);

  if(!program.args.length) {
    program.help();
  } else {
    var configFilePath = process.cwd() + '/' + program.args[0];
    readAndInitConfigs(configFilePath);
  }
})();

/***
 * Read config file and initialize all the required configurations.
 * @param configFilePath
 */
function readAndInitConfigs(configFilePath) {
  fs.readFile(configFilePath, 'utf8', function(err, configData) {
    if(err) {
      console.log('Error: ' + err);
      return;
    }
    configData = JSON.parse(configData);
    initTrelloClient(configData);
    initMailClient(configData);
    updateEmailController.getAndSendUpdateEmail(TrelloClient, EmailClient);
  });
}

/***
 * Initialize trello client
 * @param configData
 */
function initTrelloClient(configData) {
  TrelloClient = new Trello(configData.trello.appKey, configData.trello.token);
}

/***
 * Function to initialize mail client
 * @param configData
 */
function initMailClient(configData) {
  EmailClient.user = configData.mailer.auth.user;
  EmailClient.password = configData.mailer.auth.pass;
  EmailClient.scrumMasterEmail = configData.mailer.scrumMasterEmail;
  EmailClient.defaultFromAddress = configData.mailer.defaultFromAddress;
}