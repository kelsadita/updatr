'use strict';

var config = require('../Configs/config'),
  nodemailer = require('nodemailer'),
  path = require('path'),
  templatesDir = path.resolve(__dirname, '..', 'Views'),
  emailTemplates = require('email-templates'),
  defaultTransport;

var EmailAddressRequiredError = new Error('email address is required');

function initEmailConfigs(emailConfigs) {
  defaultTransport = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      user: emailConfigs.user,
      pass: emailConfigs.password
    }
  });
}

// TODO: try to use promises
exports.sendOne = function (templateName, locals, fn) {
  // make sure that we have an user email
  if (!locals.email) {
    return fn(EmailAddressRequiredError);
  }
  // make sure that we have a message
  if (!locals.subject) {
    return fn(EmailAddressRequiredError);
  }

  console.log('SENDING MAIL');

  emailTemplates(templatesDir, function(err, template) {

    if(err) {
      fn(err);
    }

    template(templateName, locals, function(err, html, text) {

      if(err) {
        return fn(err);
      }

      initEmailConfigs(locals.emailClient);

      var mailOptions = {
        from: locals.emailClient.defaultFromAddress,
        to: locals.email,
        subject: locals.subject,
        html: html,
        text: text
      };

      defaultTransport.sendMail(mailOptions, function(err, responseStatus) {
        if(err) {
          fn(err);
        }
        fn(null, responseStatus.message);
      });
    });
  });
};