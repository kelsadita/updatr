'use strict';

// Utilities
var _ = require('lodash'),
  moment = require('moment'),
  Q = require('q');


// Controllers
var mailController = require('./mailController');

// Models
var DoneItem = require('../Models/DoneItem'),
  DoingItem = require('../Models/DoingItem'),
  DailyUpdate = require('../Models/DailyUpdate');

// Trello specific variables
var TrelloResConst = require('../Constants/trelloResponseConstants'),
  TrelloBoardConst = require('../Constants/trelloBoardConstants'),
  TrelloClient;

var updateEmailController = {

  /***
   * Function to get the list of items which are done.
   * @param cardList
   */
  getDoneList: function (cardList) {
    return _.filter(cardList, function (cardItem) {
      return cardItem[TrelloResConst.LIST_ID] === TrelloBoardConst.DONE_LIST_ID;
    }).map(function (cardItem) {
      var doneSummary = cardItem[TrelloResConst.NAME];
      return new DoneItem(doneSummary);
    });
  },

  /***
   * Function to get the list of promises for inprogress items
   * @param cardList
   */
  getInProgressList: function (cardList) {

    var self = this;

    var inProgressItems = _.filter(cardList, function (cardItem) {
      return cardItem[TrelloResConst.LIST_ID] === TrelloBoardConst.DOING_LIST_ID;
    })
    .map(function (cardItem) {
      var doingSummary = cardItem[TrelloResConst.NAME];
      var doingItem = new DoingItem(doingSummary);
      doingItem.setCheckListId(cardItem.idChecklists);
      return doingItem;
    })
    .map(function (inProgressItem) {
      return self.getCheckList(inProgressItem.getCheckListId())
        .then(function (checkList) {
          inProgressItem.setCheckList(checkList.checkItems);
          return inProgressItem;
        })
        .fail(function () {
          return inProgressItem;
        });
    });

    return Q.all(inProgressItems);
  },

  /***
   * Get the check item list for inprogress card
   * @param checkListID
   * @returns {promise|*|Q.promise}
   */
  getCheckList: function (checkListID) {
    var deferred = Q.defer();
    TrelloClient.get('/1/checklists/' + checkListID, function (err, data) {
      if (err) {
        deferred.reject(err);
      }
      deferred.resolve(data);
    });

    return deferred.promise;
  },

  /***
   * Function to send daily updates
   */
  getAndSendUpdateEmail: function (TrelloAppClient, EmailClient) {

    var self = this,
        cardsData;

    // Initializing global trello client variable
    TrelloClient = TrelloAppClient;

    this.getAllCards(TrelloClient)
      .then(function (result) {

        // Populating result in carddata
        cardsData = result;

        // Get the list of items which are in-progress
        return self.getInProgressList(cardsData);

      })
      .then(function (inProgressList) {

        // Get the list of items which are done
        var doneList = self.getDoneList(cardsData);

        var dailyUpdate = new DailyUpdate(doneList, inProgressList);
        self.sendUpdateEmail(dailyUpdate, EmailClient);

      })
      .fail(function (err) {
        console.error(err);
      });
  },

  getAllCards: function () {
    var deferred = Q.defer();
    TrelloClient.get('/1/boards/' + TrelloBoardConst.SPRINT_BOARD_ID + '/cards', function (err, data) {
      if (err) {
        deferred.reject(err);
      }
      deferred.resolve(data);
    });

    return deferred.promise;
  },

  /***
   * Send update email
   * @param dailyUpdate
   * @param EmailClient
   */
  sendUpdateEmail: function (dailyUpdate, EmailClient) {

    var locals = {
      email: EmailClient.scrumMasterEmail,
      subject: 'Daily updates: ' + moment().format('MMMM Do YYYY'),
      name: 'Scrum Master',
      emailClient: EmailClient,
      doneList: dailyUpdate.doneItems,
      inProgressList: dailyUpdate.doingItems
    };

    mailController.sendOne('mailer', locals, function (err) {
      if (err) {
        console.log(err);
      }
      console.log('MAIL SENT');
    });

  }
};

module.exports = updateEmailController;